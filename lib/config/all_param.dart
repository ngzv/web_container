import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AllParam {

  /// var   定义变量、可以赋值不同类型的值，自动推断类型、没有初始化值是 nil（dynamic）
  /// final 变量，只能赋值一次、在运行时确定其值、一旦赋值后就无法再更改
  /// const 修饰常量、声明就得赋值
  /// late  声明非空变量但不初始化
  
  // 是否联网
  static bool isOnline = true;

  // 是否网络连接
  static Future<bool> isConnected() async {
    var connectivity = await (Connectivity().checkConnectivity());
    isOnline = connectivity != ConnectivityResult.none;
    return connectivity != ConnectivityResult.none;
  }

  // 获取当前版本号
  static Future<String> currentVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String current = packageInfo.version.toString();
    return current;
  }

  // 进度圆环
  Widget progress() {
    return Image.asset(
      'lib/assets/image/load.gif',
      width: 220,
      height: 220,
    );
  }
}