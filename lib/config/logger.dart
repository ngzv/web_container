import 'package:logger/logger.dart' as logger2;

class Logger {
  
  static final logger2.Logger _logger = logger2.Logger(
    printer: logger2.PrefixPrinter(logger2.PrettyPrinter()),
  );

  static void v(dynamic message) {
    _logger.v(message);
  }

  static void d(dynamic message) {
    _logger.d(message);
  }

  static void i(dynamic message) {
    _logger.i(message);
  }

  static void w(dynamic message) {
    _logger.w(message);
  }

  static void e(dynamic message) {
    _logger.e(message);
  }

  static void wtf(dynamic message) {
    _logger.wtf(message);
  }
}