// ignore_for_file: constant_identifier_names

class Urls {
  
  ///////////////////////////////////////////////////////////////////////////
  static const String protocol = 'https://';
  static const String domain = 'www.xxx.com';
  static const String port = '/';
  static const String control = 'api/';

  static const String HTTPS = protocol + domain + port + control;


  ///////////////////////////////////////////////////////////////////////////
  // 版本检测
  static const String versionUrl = HTTPS + '/versions/check';

}