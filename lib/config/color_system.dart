import 'package:flutter/material.dart';

class ColorSystem {
  
  static int backgroundColor = 0xFFFFFFFF;

  static const int clearColor = 0x00FFFFFF;
  static const int themeColor = 0xFFC52A2A;

  static const int blackFontColor = 0xFF2C2C2C;
  static const int whiteFontColor = 0xFFFFFFFF;
  static const int yellowFontColor = 0xFFFDCF42;

  void changeNight() {
    backgroundColor = 0xFF000000;
  }

  void changeDay() {
    backgroundColor = 0xFFFFFFFF;
  }

  /// `MaterialColor` 颜色自定义色板。
  /// 自定义 `MaterialColor` 我们需要传入一个颜色初始值 `primary` 及从亮到暗共十个级别的 `Map<int, Color>`。
  MaterialColor materialColor(Color color) {

    List strengths = <double>[.05];
    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    Map<int, Color> swatch = {};
    final int r = color.red, g = color.green, b = color.blue;
    // == strengths.forEach((strength) {
    for (var strength in strengths) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    }
    return MaterialColor(color.value, swatch);
  }
}