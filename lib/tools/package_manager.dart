import 'package:package_info/package_info.dart';

class PackageManager {

  /// 版本号
  static Future<String> version() async {

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version.toString();
    return version;
  }

  /// 内部版本号
  static Future<String> build() async {
    
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String buildNumber = packageInfo.buildNumber.toString();
    return buildNumber;
  }
}