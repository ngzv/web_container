// ignore: import_of_legacy_library_into_null_safe
import 'package:event_bus/event_bus.dart';

/*
 * 单例模式创建EventBus工具类
 * EventBusUtils.instance().fire(IntContentEvent(1));
 * 
 * EventBusUtils.instance().on<IntContentEvent>().listen((event) {
 *   print(event.value.toString());
 * });
 */
class EventBusUtils {
  
  static EventBus instance() {
    EventBus instce = EventBus();
    return instce;
  }
}

/*
 * 创建事件，这个事件其实就是一个用来承载共享数据的载体
 */
class StringContentEvent {

  String value;
  StringContentEvent(this.value);
}

class IntContentEvent {

  int value;
  IntContentEvent(this.value);
}

class DoubleContentEvent {

  double value;
  DoubleContentEvent(this.value);
}