import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:web_container/config/color_system.dart';
import 'package:web_container/pages/root/splash_page.dart';

Future main() async {

  /// `Flutter 1.7.8+` 版本必须加，否则报错
  WidgetsFlutterBinding.ensureInitialized();
  /// 设置 `Android` 状态栏为透明的沉浸
  /// 写在组件渲染之后，是为了在渲染后进行 `set` 赋值，覆盖状态栏，写在渲染之前 `MaterialApp` 组件会覆盖掉这个值
  /// 底色透明是否生效与 `Android` 版本有关，版本过低设置无效
  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FLUTTER WEBVIEW',
      /// 隐藏 `debug` 标识
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        //
        /// 设置主题色
        // primarySwatch: Colors.blue,
        primaryColor: const Color(ColorSystem.themeColor),
        /// 设置 `item` 点击、放开颜色（即取消水波纹效果）
        splashColor: const Color.fromRGBO(1, 0, 0, 0.0),
        highlightColor: const Color.fromRGBO(1, 0, 0, 0.0),
      ),
      /// 进入的主页面
      home: const SplashPage(),
    );
  }
}
