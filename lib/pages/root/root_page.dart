import 'package:flutter/material.dart';
import 'package:web_container/pages/universal/web_view_page.dart';

class RootPage extends StatefulWidget {

  const RootPage({Key? key}): super(key: key);

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
    // 跳转到新页面，并等待该页面pop返回
    // Navigator.push(context, MaterialPageRoute(builder: (context) => const WebViewPage()));
    // 替换当前页面并跳转到新页面
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const WebViewPage()));
    // 跳转到新页面并移除当前页面
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const WebViewPage(
      title: 'WEBVIEW',
      url: 'https://www.baidu.com',
    )), (Route<dynamic> route) => false);
  }

  @override
  void initState() {    
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text('启动广告'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
