import 'package:flutter/material.dart';
import 'package:web_container/pages/root/root_page.dart';

class GuidePage extends StatefulWidget {

  const GuidePage({Key? key}): super(key: key);

  @override
  State<GuidePage> createState() => _GuidePageState();
}

class _GuidePageState extends State<GuidePage> 
    with SingleTickerProviderStateMixin {

  // 引出带动画的 `widget`
  AnimationPage animationPage = AnimationPage();

  // 翻页事件
  void startPagePaged(int page) {
    // 当到指定页面时触发动画
    if (page == 3) {
      animationPage.button.startAnimation();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget> [
        // 设置为白色背景
        Container(
          color: Colors.white,
        ),
        // 添加一个 PageView
        PageView(
          // 设置为水平滚动并添加素材
          scrollDirection: Axis.horizontal,
          children: <Widget> [
            Image.asset('lib/assets/image/guide.png', fit: BoxFit.fill,),
            Image.asset('lib/assets/image/guide.png', fit: BoxFit.fill,),
            Image.asset('lib/assets/image/guide.png', fit: BoxFit.fill,),
            // Image(image: AssetImage('lib/assets/image/guide.png', fit: BoxFit.fill,)),
            animationPage,
          ],
          // 翻页事件
          onPageChanged: startPagePaged,
        ),
      ],
    );
  }
}


// 动画页面
// ignore: must_be_immutable
class AnimationPage extends StatefulWidget {

  AnimationPage({Key? key}): super(key: key);

  // 设置一个标题
  AnimationButton button = AnimationButton(title: '开启新版',);

  @override
  State<AnimationPage> createState() => _AnimationPageState();
}

class _AnimationPageState extends State<AnimationPage> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      // 中心对齐
      alignment: Alignment.center,
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: const Image(
            image: AssetImage('lib/assets/image/guide.png'),
            fit: BoxFit.fill,
          ),
        ),

        Positioned(
          // 设置一个位置,距离底部 `60`
          bottom: 120,
          child: widget.button
       )
      ],
    );
  }
}


// 动画按钮
// ignore: must_be_immutable
class AnimationButton extends StatefulWidget {
  
  // 按钮标题
  String title;
  late AnimationController controller;
  late Animation<double> animation;

  void startAnimation() {
    // ignore: unnecessary_null_comparison
    if (controller == null) {
      return;
    }
    controller.forward();
  }

  AnimationButton({
    Key? key,
    required this.title,
  }): super(key: key);

  @override
  State<AnimationButton> createState() => _AnimationButtonState();
}

class _AnimationButtonState extends State<AnimationButton> 
    with SingleTickerProviderStateMixin {

  @override
  void dispose() {
    super.dispose();
    // 路由销毁时需要释放动画资源
    widget.controller.dispose();
  }

  @override
  void initState() {
    super.initState();
    // 初始化动画控制器和动画
    // 配置控制器 动画持续时间
    widget.controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    // 动画描述，在持续时间内值的变化
    widget.animation = Tween(
      begin: 0.0, 
      end: 60.0,
      ).animate(widget.controller)..addListener((){
        // 值发生变化后 `setSatate` 即可
        setState(() {});
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {
        /// 点击之后保存状态
        // 跳转到新页面，并等待该页面pop返回
        // Navigator.push(context, MaterialPageRoute(builder: (context) => const RootPage()));
        // 替换当前页面并跳转到新页面
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const RootPage()));
        // 跳转到新页面并移除当前页面
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const RootPage()), (Route<dynamic> route) => false);
      },
      // 动画开始后在控制器中设定的时间内会不断调用 `addListener` 传递的方法,
      // 并且 `widget.animation.value` 会在持续时间内按照设定的开始和结束均匀变化,利用这个值设定渐变,位移等即可
      child: Text(
        widget.title, 
        style: TextStyle(color: Color.fromRGBO(255, 82, 82, widget.animation.value / 60),),
      ),
      style: ButtonStyle(
        side: MaterialStateProperty.all(
          BorderSide(
            width: 1.0,
            color: Color.fromRGBO(255, 82, 82, widget.animation.value / 60),
          ),
        ),
      ),
    );
  }
}
